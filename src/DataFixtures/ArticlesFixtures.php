<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use App\Entity\Comment;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class ArticlesFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        //Créer 3 catégories fakées
        for ($i = 1; $i <= 3; $i++) {
            $category = new Category();
            $category->setTitle($faker->sentence())
                ->setDescription($faker->paragraph());

            $manager->persist($category);

            //Créer entre 4 et 6 articles

            for ($j = 1; $j <= mt_rand(4, 6); $j++) {
                $article = new Article();

                $content = '<p>';
                $content .= join($faker->paragraphs(5), '</p><p>');
                $content .= '</p>';

                $article->setTitle($faker->sentence())
                    ->setContent($content)
                    ->setImage($faker->imageUrl())
                    ->setCreatedAt($faker->dateTimeBetween('-6 months'))
                    ->setCategory($category);

                $manager->persist($article);

                //Créer entre 4 et 10 comments
                for ($k = 1; $k <= mt_rand(4, 10); $k++) {
                    $comment = new Comment();

                    $content = '<p>';
                    $content .= join($faker->paragraphs(2), '</p><p>');
                    $content .= '</p>';

                    //On fait un commentaire entre la date du jour et la date de creation de l'article
                    $now = new \DateTime();//date du jour
                    //on récupère la différence entre les 2 objets Datetime
                    $interval = $now->diff($article->getCreatedAt());
                    //on récupère le nombre de jour
                    $days = $interval->days;
                    $minim = '-' . $days . 'days'; //example: -100 days

                    $comment->setAuthor($faker->name())
                        ->setContent($content)
                        ->setCreatedAt($faker->dateTimeBetween($minim))
                        ->setArticle($article);

                    $manager->persist($comment);
                }
            }
        }

        $manager->flush();
    }
}
